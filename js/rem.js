(function (win, doc) {
  var pwidth = 1920, prem = 100;
  win.addEventListener('load', function () {
    getRem(pwidth, prem)
  });
  win.addEventListener('resize', function () {
    getRem(pwidth, prem)
  });

  function getRem(pwidth, prem) {
    var html = doc.getElementsByTagName("html")[0];
    var oWidth = doc.body.clientWidth || doc.documentElement.clientWidth;
    html.style.fontSize = oWidth / pwidth * prem + "px";
  }

  getRem(pwidth, prem)
})(window, document);
