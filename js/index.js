// 全局轮播
var numPage = 0;
new Swiper('.main-wrapper', {
  direction: 'vertical',
  loop: true,
  autoplay: {
    delay: 1000,
    stopOnLastSlide: false,
    disableOnInteraction: true,
  },
  autoplay: false,
  touchStartPreventDefault: false, // 阻止touchstart
  initialSlide: 0, // 当前屏幕位置
  observer: true,//修改swiper自己或子元素时，自动初始化swiper
  observeParents: true,//修改swiper的父元素时，自动初始化swiper
  on: {
    slideChangeTransitionEnd: function () {//切换完执行的方法
      numPage = this.activeIndex;//当前是第几个页面
      if (numPage == 1 || numPage == 3) {
        $(".left-ewm").hide()
        $(".fwl").show()
        $(".one-left-b").show()
      }
      if (numPage == 2) {
        $(".left-ewm").show()
        $(".fwl").hide()
        $(".one-left-b").hide()
      }
      console.log(numPage)
    }
  }
});
setInterval(function () {
  setNumberRun1(58963218)
}, 3000)
// 翻牌器
function setNumberRun1(n) {
  var num1 = document.querySelectorAll('.numberRun1')
  for (i of num1) {
    $(i).numberAnimate({
      num: n,
      dot: 0,
      speed: 2000,
    });
  }
}
// 证书动画
function f() {
  setTimeout(function () {
    document.querySelector('.zs-img').classList.add('slideOutRight')
    f1()
  }, 3000);
}
function f1() {
  setTimeout(function () {
    document.querySelector('.zs-img').classList.remove('slideOutRight')
    f()
  }, 1000);
}
f()
// 证书动画end

// 底部证书轮播
var oUl = document.querySelector('.zs-main ul');
var aLi = document.querySelectorAll('.zs-main li');
var speed = -1;
var timer = null;
// oUl.innerHTML += oUl.innerHTML;


function timere() {
  oUl.style.width = aLi[0].offsetWidth * aLi.length + 'px';
  timer = setInterval(function () {
    oUl.style.left = oUl.offsetLeft + speed + 'px';
    if (oUl.offsetLeft < -oUl.offsetWidth / 2) {
      oUl.style.left = '0';
    } else if (oUl.offsetLeft > 0) {
      oUl.style.left = -oUl.offsetWidth / 2 + 'px';
    }
  }, 30);
}
// 证书轮播end

var htmlSize = parseInt(document.querySelector('html').style.fontSize) * 0.61;
$('.llist').myScroll({
  speed: 50, //数值越大，速度越慢
  rowHeight: htmlSize //li的高度
});




// 第一屏
var dataArr = []
function getDataAjax() {
  return new Promise((reject, resolve) => {
    $.ajax({
      url: './1.json',
      type: 'get',
      async: false,
      dataType: 'json',
      success: function (data) {
        reject(data)
      }
    })
  })
}
doSth()
// 进行
async function doSth() {
  const data = await getDataAjax()
  dataArr = data
  logic(dataArr)
  setTimeout(doSth, 17000)
  oUl.style.transform = 'translateX(0)'
  oUl.classList.remove('animate')
  setTimeout(function () {
    oUl.classList.add('animate')
  }, 10)
  oUl.style.left = '0';
}
function logic(data) {
  var ccArr = []
  for (var i = 0; i < data.data.length; i++) {
    var thenData = data.data[i];
    ccArr += `
      <li>
        <img src="${thenData.img}" alt="">
        <p>${thenData.name}</p>
      </li>
    `
  }
  setNumberRun1(data.fwl)
  $(".swiper1 .time").html(data.time)
  $(".zs-main ul").html(ccArr)
  // $(".right-zsAll span").html(data.jkr)
  aLi = document.querySelectorAll('.zs-main li');
  // oUl.innerHTML += oUl.innerHTML;
  clearInterval(timer)
  timere()
}

// 第二屏
function ajaxUl() {
  $.ajax({
    url: './2.json',
    type: 'get',
    async: false,
    dataType: 'json',
    success: function (data) {
      // $("#jine").html(data.yuan)
      // $("#renc").html(data.renc)
      // setNumberRun2(data.yuan)
      // setNumberRun3(data.renc)
      var dataArr = data.data
      doTwoSth(dataArr)
    }
  })
  setTimeout(ajaxUl, 10000)
}
ajaxUl()


function doTwoSth(data) {
  var dataCc = []
  for (var i = 0; i < data.length; i++) {
    dataCc += `
          <li class="box-tr">
            <div class="name">${data[i].name}</div>
            <div class="je">${data[i].jine} <span>元</span></div>
            <p class="xm">${data[i].cany}</p>
            <div class="time">${data[i].time}</div>
          </li>
        `
  }
  $(".box-tabel").append(dataCc)
}